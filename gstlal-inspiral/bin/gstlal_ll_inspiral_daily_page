#!/usr/bin/env python
#
# Copyright (C) 2015  Kipp Cannon, Chad Hanna
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

### A program to generate offline style web pages for an online analysis

"""Meta program to generate offline style summary pages from online runs."""


import sys, os, subprocess, re, time, glob, shutil
from optparse import OptionParser
from glue.text_progress_bar import ProgressBar
from gstlal import aggregator
import lal
from lal import LIGOTimeGPS
from multiprocessing import Pool
from gstlal import dagparts
from copy import copy

def now():
	return LIGOTimeGPS(lal.UTCToGPS(time.gmtime()))

def process_mass_bin(args): 
	massbin, result_dirs, n, d, options, tag, typecode, cluster_file = args

	print >> sys.stderr, "processing mass bin %s, tag %s in directory %d of %d: %s" % (massbin, tag, n+1, len(result_dirs), d)

	# FIXME don't hard code H1L1
	# FIXME assumes 10 digit GPS
	db = os.path.join(os.path.join(options.directory, d), 'H1L1-%s_%s-%s00000-100000.sqlite.tmp' % (massbin, tag, d))
	if os.path.exists(db):
		os.remove(db)
	dbfinal = db.replace(".tmp","")

	# FIXME we really have to change this hacky convention for injections to start at 1000
	pattern = re.compile('.*-%d%s_LLOID-.*.xml.gz' % (typecode, massbin[1:]))

	# First do non injections
	files = sorted([os.path.join(os.path.join(options.directory, d), xml) for xml in os.listdir(os.path.join(options.directory, d)) if pattern.match(xml) and "~" not in xml])
	for f in files:
		try:
			subprocess.check_call(["gstlal_inspiral_merge_and_reduce", "--sql-file", cluster_file, "--tmp-space", "/dev/shm", "--database", "%s" % db, "%s" % f])
		except:
			print >> sys.stderr, "couldn't process %s" % f
			continue

	# rename files
	if os.path.exists(db):
		os.rename(db, dbfinal)

	return dbfinal	

def injtag(injection_file):
	return os.path.split(injection_file)[1].split('.')[0].replace("-","_")

if __name__ == '__main__':

	#
	# Some data reduction happens in parallel
	#

	pool = Pool(8)

	#
	# Parse command line
	#

	parser = OptionParser()
	parser.add_option("--directory", default = ".", help = "This option is disabled it will be ignored. It is set to '.' ")
	parser.add_option("--injection-file", default = [], action = "append", help = "The injection xml files that corresponds to the low latency injections for given mass bins. 0000:0002:Injection_1.xml, 0002:0004:Injection_2.xml")
	parser.add_option("--web-dir", help = "set the output path to write the ''offline'' style web page to")
	options, massbins = parser.parse_args()
	options.directory = "." # only relative paths are supported at the moment.

	# FIXME hardcoded instruments
	instruments = "H1L1"

	# FIXME should be more clever than this
	# Match 5 digit directories
	dir_pattern = re.compile('[0-9]{5}')

	noninj_files_to_merge = []
	inj_files_to_merge = {}
	inj_file_bins = {}
	injdball = {}

	for injection_file in options.injection_file:
		inj_file_split = injection_file.split(':')
		for massbin in range(int(inj_file_split[0]),int(inj_file_split[1])):
			inj_file_bins.setdefault(inj_file_split[2],[]).append(str(massbin).zfill(4))

	# FIXME assume that the correct low latency cluster file is in the working
	# directory. Perhaps this should be a command line argument.
	cluster_file = os.path.join(options.directory, "ll_simplify_and_cluster.sql")
	# FIXME presently not used, should it be?
	simplify_file = os.path.join(options.directory, "ll_simplify.sql")

	#
	# Parallel process data within each result directory (every 100,000 seconds)
	#

	result_dirs = sorted([d for d in os.listdir(options.directory) if dir_pattern.match(d)])
	for n, d in enumerate(result_dirs):

		noninjdball = os.path.join(os.path.join(options.directory, d), 'H1L1-ALL_LLOID-%s00000-100000.sqlite' % (d,))
		for injection_file in inj_file_bins:
			injdball[injection_file] = os.path.join(os.path.join(options.directory, d), dagparts.T050017_filename(instruments, "ALL_LLOID_%s" % injtag(injection_file), (int(d) * 100000, (int(d) + 1) * 100000), '.sqlite'))

		if float(now()) - float("%s00000" % d) > 125000 and all([os.path.exists(f) for f in injdball.values()]+[os.path.exists(noninjdball)]):
			print >> sys.stderr, "directory is %s %s greater than 125000 seconds old and has already been processed...continuing" % (n,d)
			noninj_files_to_merge.append(noninjdball)
			for injection_file in inj_file_bins:
				inj_files_to_merge.setdefault(injection_file,[]).append(injdball[injection_file])
			continue

		# Parallel process the data reduction
		args = ([massbin, result_dirs, n, d, options, "ALL_LLOID", 0, cluster_file] for massbin in massbins)
		# Merge the files of this directory
		subprocess.check_call(["gstlal_inspiral_merge_and_reduce", "--sql-file", cluster_file, "--tmp-space", "/dev/shm", "--replace", "--verbose", "--database", "%s" % noninjdball] + list(pool.map(process_mass_bin, args)))
		noninj_files_to_merge.append(noninjdball)

		for injection_file in inj_file_bins:
			args = ([massbin, result_dirs, n, d, options, injtag(injection_file), 1, cluster_file] for massbin in inj_file_bins[injection_file])
			subprocess.check_call(["gstlal_inspiral_merge_and_reduce", "--sql-file", cluster_file, "--tmp-space", "/dev/shm", "--replace", "--verbose", "--database", "%s" % injdball[injection_file]] + list(pool.map(process_mass_bin, args)))
			inj_files_to_merge.setdefault(injection_file,[]).append(injdball[injection_file])

	#
	# Do top level data reduction
	#

	# FIXME only add *new* files
	noninjdb = os.path.join(options.directory, 'H1L1-ALL_LLOID-0-2000000000.sqlite.tmp')

        injdb = {}
	for injection_file in inj_file_bins:
		injdb[injection_file] = os.path.join(options.directory, 'H1L1-ALL_LLOID_%s-0-2000000000.sqlite.tmp' % (injtag(injection_file)))

	if os.path.exists(noninjdb):
		os.remove(noninjdb)

	for injection_file in injdb:
		if os.path.exists(injdb[injection_file]):
			os.remove(injdb[injection_file])

	progressbar = ProgressBar("Merge noninjection files", len(noninj_files_to_merge))
	for f in noninj_files_to_merge:
		# NOTE the online analysis doesn't do a global clustering stage!! That means that you will under count the events in the final db
		subprocess.check_call(["gstlal_inspiral_merge_and_reduce", "--sql-file", cluster_file, "--tmp-space", "/dev/shm", "--verbose", "--database", "%s" % noninjdb, "%s" % f])
		progressbar.increment()
	del progressbar

	for injection_file in inj_file_bins:
		progressbar = ProgressBar("Merge injection files", len(inj_files_to_merge[injection_file]))
		for f in inj_files_to_merge[injection_file]:
			# NOTE the online analysis doesn't do a global clustering stage!! That means that you will under count the events in the final db
			subprocess.check_call(["gstlal_inspiral_merge_and_reduce", "--sql-file", cluster_file, "--tmp-space", "/dev/shm", "--verbose", "--database", "%s" % injdb[injection_file], "%s" % f])
			progressbar.increment()
		del progressbar

		# Find injections
		progressbar = ProgressBar("Find injections", 4)
		subprocess.check_call(["ligolw_sqlite", "--tmp-space", os.environ["TMPDIR"], "--verbose", "--database", "%s" % injdb[injection_file], "%s" % injection_file])
		progressbar.increment()
		subprocess.check_call(["ligolw_sqlite", "--tmp-space", os.environ["TMPDIR"], "--verbose", "--database", "%s" % injdb[injection_file], "--extract", "%s.xml" % injdb[injection_file]])
		progressbar.increment()
		subprocess.check_call(["lalapps_inspinjfind", "--verbose", "%s.xml" % injdb[injection_file]])
		progressbar.increment()
		subprocess.check_call(["ligolw_sqlite", "--tmp-space", os.environ["TMPDIR"], "--verbose", "--database", "%s" % injdb[injection_file], "--replace", "%s.xml" % injdb[injection_file]])
		progressbar.increment()

	#
	# Make plots and such
	#

	plot_dir = os.path.join(options.directory, "plots")
	if os.path.exists(plot_dir):
		os.rename(plot_dir, "%s.%s" % (plot_dir, str(now())))
	os.mkdir(plot_dir)

	pattern = re.compile("(?P<id>[0-9]{4})_prior.xml.gz")
	#for d in os.listdir(options.directory):
	#	m = pattern.match(d)
	#	if m:
	#		subprocess.check_call(["gstlal_inspiral_plot_background", "--output-dir", plot_dir, "--user-tag", m.group("id"), "--verbose", d])

	#
	# Plot background
	#

	# FIXME this is broken for some reason online
#	try:
#		subprocess.check_call(["gstlal_inspiral_plot_background", "--output-dir", plot_dir, "--user-tag", "ALL", "--database", noninjdb, "--verbose", "rankingstat_pdf.xml.gz"])
#	except subprocess.CalledProcessError as plot_error:
#		print >> sys.stderr, "plotting failed. recieved error%s...continuing anyway" % plot_error

	#
	# Plot summary
	#

	try:
		if inj_file_bins:
			subprocess.check_call(["gstlal_inspiral_plotsummary", "--verbose", "--likelihood-file", "rankingstat_pdf.xml.gz", "--segments-name", "statevectorsegments", "--user-tag", "ALL_LLOID_COMBINED", "--output-dir", plot_dir, "%s" % noninjdb] + injdb.values())
		else:
			subprocess.check_call(["gstlal_inspiral_plotsummary", "--verbose", "--likelihood-file", "rankingstat_pdf.xml.gz", "--segments-name", "statevectorsegments", "--user-tag", "ALL_LLOID_COMBINED", "--output-dir", plot_dir, "--plot-group", "0", "--plot-group", "5", "%s" % noninjdb])
	except subprocess.CalledProcessError as plot_error:
		print >> sys.stderr, "plotting failed. recieved error%s...continuing anyway" % plot_error

	#
	# Plot sensitivity
	#

	if inj_file_bins:
		try:
			subprocess.check_call(["gstlal_inspiral_plot_sensitivity", "--verbose", "--output-dir", plot_dir, "--bin-by-source-type", "--zero-lag-database", noninjdb, "--dist-bins", "200",  "--user-tag", "ALL_LLOID_COMBINED", "--data-segments-name", "statevectorsegments"] + injdb.values())
		except subprocess.CalledProcessError as plot_error:
			print >> sys.stderr, "plotting failed. recieved error%s...continuing anyway" % plot_error

	#
	# Summary page
	#

	try:
		webserverplotdir = os.path.join(options.web_dir, "plots")
		shutil.rmtree(options.web_dir + "_lite", ignore_errors = True)
		if os.path.exists(webserverplotdir):
			newdir = os.path.join(options.web_dir, str(int(now())))
			aggregator.makedir(newdir)
			shutil.move(webserverplotdir, newdir)
			try:
				shutil.move(os.path.join(options.web_dir, "index.html"), newdir)
			except IOError:
				pass

		# FIXME only relative paths are supported hence the hardcoded "plots" directory
		subprocess.check_call(["gstlal_inspiral_summary_page_lite", "--no-navigation", "--open-box", "--output-user-tag", "ALL_LLOID_COMBINED",  "--glob-path", "plots", "--webserver-dir", options.web_dir, "--title", "gstlal-online"])
	except subprocess.CalledProcessError as plot_error:
		print >> sys.stderr, "plotting failed. recieved error%s...continuing anyway" % plot_error


	# copy the working files back
	os.rename(noninjdb, noninjdb.replace(".tmp",""))
	for injection_file in injdb:
		os.rename(injdb[injection_file], injdb[injection_file].replace(".tmp",""))

